package ahct.notices.soapdemo;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.adobe.idp.services.BLOB;
import com.adobe.idp.services.Invoke;
import com.adobe.idp.services.InvokeRequest;
import com.adobe.idp.services.InvokeResponse;

import net.minidev.json.writer.BeansMapper.Bean;

@Endpoint
public class NoticeEndPoints {

	private static final String NAMESPACE_URI = "http://adobe.com/idp/services";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "InvokeRequest")
	@ResponsePayload
	public InvokeResponse get1301NoticeFromXML(@RequestPayload InvokeRequest request) throws IOException {
		Invoke invoke = request.getInvoke();
		if(invoke.getOutNotice().getBinaryData() != null){
		byte[] xmlBlob = invoke.getOutNotice().getBinaryData();
		File xmlTempFile = new File("xmlStream.xml");
		//FileUtils.copyInputStreamToFile(xmlStream, xmlTempFile);
		ByteArrayInputStream xmlStream =  new ByteArrayInputStream(xmlBlob);
		OutputStream outputStreamXML = new FileOutputStream(xmlTempFile);
		IOUtils.copy(xmlStream, outputStreamXML);
		outputStreamXML.close();
		}else{
			System.out.println("BinaryData is coming as null.");
		}
		InvokeResponse invokeResponse = new InvokeResponse();
		BeanUtils.copyProperties(invoke, invokeResponse);
		if(invoke.getOutNotice().getBinaryData() == null){
			BLOB blobtemp=	new BLOB();
			blobtemp.setAttachmentID("ABC");
		invokeResponse.setOutNotice(blobtemp);
		}
		invokeResponse.setPageCount("Testing");
		invokeResponse.setSheetCount("Sheet count");
		//invokeResponse.setOutNotice(new AhctNoticeSoapdemoXmlModelsBLOB());
		return invokeResponse;
	}
}
